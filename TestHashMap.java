import java.util.Map;

/**Main class
 * Create a HashMap object.
 * Testing get(),remove(), put() methods.
 * @author Anna Derkach
 * @version 1.0
 */
public class TestHashMap {
	public static void main(String[] args) {
		HashMap<String, Integer> map = new HashMap<String, Integer>();
		map.put("1", 3);
		map.put("2", 2);
		map.put("3", 3);
		System.out.println(map.get("2"));
		System.out.println(map.get("1"));
		System.out.println(map.get("3"));
		map.remove("2");
	}
}

/**HashMap class
 * Own library implementation.  
 */
class HashMap<K, V> implements Map.Entry<K, V> {
	/**The default initial capacity - MUST be a power of two.*/
	public static int DEFAULT_CAPACITY = 16;
	/**The maximum capacity, used if a higher 
	 * value is implicitly specified by either 
	 * of the constructors with arguments. 
	 * MUST be a power of two <= 1<<30.
	*/
	private static final int MAX_CAPACITY = 1073741824;
	/**The load factor used when none specified in constructor.*/
	static final float DEFAULT_LOAD_FACTOR = 0.75f;
	
	int threshold;
	int capacity;
	/**The next size value at which to resize (capacity * load factor).*/
	final float loadFactor;
	/**The table, resized as necessary. Length MUST Always be a power of two.*/
	public Entry<K, V>[] table;
	/**Determines ability to remove an item*/
	private boolean deleted[];
	int flag = 0;
	
	/** Constructs an empty HashMap with
	 *  the default initial capacity (16)
	 *   and the default load factor (0.75).
	*/
	HashMap() {
		table = new Entry[DEFAULT_CAPACITY];
		capacity = DEFAULT_CAPACITY;
		deleted = new boolean[DEFAULT_CAPACITY];
		this.loadFactor = DEFAULT_LOAD_FACTOR;
		threshold = (int) (DEFAULT_CAPACITY * DEFAULT_LOAD_FACTOR);
	}
	
	/** Define the item position in the table*/
	public int indexFor(K key) {
		return (hashOne(key.hashCode()) + hashTwo(key.hashCode())) % capacity
				+ 1;
	}

	public int hashOne(int hash) {
		return hash % (capacity + 1);
	}

	public int hashTwo(int hash) {
		return hash % (capacity - 1) + 1;
	}
	/**Associates the specified value with
	 *  the specified key in this map.
	*/
	public void put(K key, V value) {
		flag++;
		int x = hashOne(key.hashCode());
		int y = hashTwo(key.hashCode());
		for (int i = 0; i < table.length; i++) {
			if (table[x] == null || deleted[x] == true) {
				Entry<K, V> e = table[x];
				table[x] = new Entry<K, V>(key.hashCode(), key, value, e);
				deleted[x] = false;
				break;
			}
			x = (x + y) % capacity;
		}
		if (flag < capacity)
			return;
		else if (flag == capacity)
			resize(2 * table.length);
	}
	/**Removes the mapping for the specified
	 *  key from this map if present.
	*/
	void remove(Object key) {
		int x = hashOne(key.hashCode());
		int y = hashTwo(key.hashCode());
		for (int i = 0; i < table.length; i++) {
			if (table[x] != null)
				if (table[x].key == key)
					deleted[x] = true;
				else
					return;
		}
		x = (x + y) % capacity;
	}
	
	/**Returns the value to which the specified
	 *  key is mapped, or null if this map 
	 *  contains no mapping for the key.
	*/
	public V get(Object key) {
		int x = hashOne(key.hashCode());
		int y = hashTwo(key.hashCode());
		for (int i = 0; i < table.length; i++) {
			if (table[x] != null)
				if (table[x].key == key && deleted[x] == false)
					return (V) table[x].value;
				else
					return null;
			x = (x + y) % capacity;

		}
		return null;
	}
	/**Transfers all entries from current table to newTable.*/
	void transfer(Entry<K, V>[] newTable) {
		Entry<K, V>[] src = table;
		int newCapacity = newTable.length;
		for (int j = 0; j < src.length; j++) {
			Entry<K, V> e = src[j];
			if (e != null) {
				src[j] = null;
				do {
					Entry<K, V> next = e.next;
					int i = indexFor(e.key);
					e.next = newTable[i];
					newTable[i] = e;
					e = next;
				} while (e != null);
			}
		}

	}
	/**Rehashes the contents of this map into a new
	 *  array with a larger capacity. 
	 *  This method is called automatically when
	 *   the number of keys in this map reaches its 
	 *   threshold.*/
	void resize(int newCapacity) {
		Entry<K, V>[] oldTable = table;
		int oldCapacity = oldTable.length;
		if (oldCapacity == MAX_CAPACITY) {
			threshold = Integer.MAX_VALUE;
			return;
		}
		@SuppressWarnings("unchecked")
		Entry<K, V>[] newTable = new Entry[newCapacity];
		transfer(newTable);
		table = newTable;
		threshold = (int) (newCapacity * loadFactor);
	}

	@Override
	public V getValue() {
		return null;
	}

	@Override
	public V setValue(V value) {
		return null;
	}

	@Override
	public K getKey() {
		return null;
	}

}

class Entry<K, V> {
	K key;
	V value;
	Entry<K, V> next;
	final int hash;
/**Creates new entry.*/
	Entry(int h, K k, V v, Entry<K, V> n) {
		value = v;
		next = n;
		key = k;
		hash = h;
	}

}